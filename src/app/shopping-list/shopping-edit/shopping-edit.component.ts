import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IngredientModel } from "../../shared/ingredient.model";
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { Store } from "@ngrx/store";

import * as fromApp from '../../store/app.reducer';
import * as ShoppingListActions from "../store/shopping-list.actions";


@Component({
    selector: 'app-shopping-edit',
    templateUrl: './shopping-edit.component.html',
    styleUrls: ['./shopping-edit.component.scss']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

    @ViewChild('f', {static: false}) shoppingListForm: NgForm;
    subscription: Subscription;
    editMode = false;
    editItemIndex: number;
    editItem: IngredientModel;

    constructor(
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit(): void {
        this.subscription = this.store
            .select('shoppingList')
            .subscribe(stateData => {
                if (stateData.editedIngredientIndex > -1) {
                    this.editMode = true;
                    this.editItem = stateData.editedIngredient;
                    this.shoppingListForm.setValue({
                        name: this.editItem.name,
                        amount: this.editItem.amount
                    });
                } else {
                    this.editMode = false;
                }
            });

    }

    onFormSubmit(form: NgForm) {
        const value = form.value;
        const newIngredient = new IngredientModel(value.name, value.amount);
        if (this.editMode) {
            this.store.dispatch(
                new ShoppingListActions.UpdateIngredient(newIngredient)
            );
        } else {
            this.store.dispatch(new ShoppingListActions.AddIngredient(newIngredient));
        }
        this.editMode = false;
        form.reset();
    }

    onClear() {
        this.shoppingListForm.reset();
        this.editMode = false;
        this.store.dispatch(new ShoppingListActions.StopEdit());
    }

    onDelete() {
        this.store.dispatch(new ShoppingListActions.DeleteIngredient());
        this.onClear();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.store.dispatch(new ShoppingListActions.StopEdit());
    }

}
