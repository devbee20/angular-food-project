import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from "rxjs";
import { Store } from "@ngrx/store";

import {IngredientModel} from "../shared/ingredient.model";
import * as fromApp from '../store/app.reducer';
import * as ShoppingListActions  from "./store/shopping-list.actions";


@Component({
    selector: 'app-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls: ['./shopping-list.component.scss'],
    providers: []
})
export class ShoppingListComponent implements OnInit, OnDestroy {

    ingredients: Observable<{ ingredients: IngredientModel[] }>;
    private subscription: Subscription;

    constructor(
        private store: Store<fromApp.AppState>
    ) {
    }

    ngOnInit(): void {
        this.ingredients = this.store.select('shoppingList');
    }


    ngOnDestroy(): void {
       // this.subscription.unsubscribe();
    }

    onEditItem (index: number) {
        this.store.dispatch(new ShoppingListActions.StartEdit(index));
    }

}
