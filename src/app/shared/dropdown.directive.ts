import {Directive, ElementRef, HostListener} from "@angular/core";

@Directive({
    selector: '[appDropDown]'
})
export class DropdownDirective {

    constructor(private elRef: ElementRef) {

    }

    @HostListener('click') toggleOpen() {

        const childDropDown = this.elRef.nativeElement.querySelector('.dropdown-menu');
        childDropDown.classList.toggle('show');
    }

}
