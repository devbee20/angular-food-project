import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PlaceholderDirective } from "./placeholder/placeholder.directive";
import { LoadingSpinnerComponent } from "./loading-spinner/loading-spinner.component";
import { AlertComponent } from "./alert/alert.component";
import { DropdownDirective } from "./dropdown.directive";

@NgModule({
    declarations: [
        PlaceholderDirective,
        LoadingSpinnerComponent,
        AlertComponent,
        DropdownDirective

    ],
    imports: [
        CommonModule
    ],
    exports: [
        PlaceholderDirective,
        AlertComponent,
        LoadingSpinnerComponent,
        DropdownDirective,
        CommonModule
    ],
    entryComponents: [AlertComponent]
})

export class SharedModule {

}
