/*
  Firebase setup config
  Paste your own config object into a file called firebase.config.ts in the app folder.
*/


// Initialize Firebase
export let firebaseConfig = {
  firebaseAPIKey: ' ',
  authDomain:  ' ',
  projectDomain: ' ',
};
