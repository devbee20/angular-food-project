import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { map } from "rxjs/operators";

import { RecipeModel } from "../recipe.model";
import * as fromApp from '../../store/app.reducer';
import * as RecipesActions from "../store/recipe.actions";


@Component({
    selector: 'app-recipe-list',
    templateUrl: './recipe-list.component.html',
    styleUrls: ['./recipe-list.component.scss'],
})
export class RecipeListComponent implements OnInit, OnDestroy {

    recipes: RecipeModel[];
    subscription: Subscription;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private store: Store<fromApp.AppState>) {
    }
// todo add fetch recipes
    ngOnInit(): void {
        this.store.dispatch(new RecipesActions.FetchRecipes());
        this.subscription = this.store
            .select('recipes')
            .pipe(map(recipesState => recipesState.recipes))
            .subscribe(
                (recipes: RecipeModel[]) => {
                    this.recipes = recipes;
                }
            );
    }

    onNewRecipe() {
        this.router.navigate(['create'], {relativeTo: this.route});
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
