import { Action } from "@ngrx/store";
import { RecipeModel } from "../recipe.model";

export const SET_RECIPES = '[Recipes] Set Recipes';
export const FETCH_RECIPES = '[Recipes] Fetch Recipes';
export const STORE_RECIPES = '[Recipe] Store Recipes';
export const DELETE_RECIPE = '[Recipe] Delete Recipe';
export const ADD_RECIPE = '[Recipe] Add Recipe';
export const UPDATE_RECIPE = '[Recipe] Update Recipe';

export class SetRecipes implements Action {

    readonly type = SET_RECIPES;
    constructor(public payload: RecipeModel[]) {
    }
}

export class FetchRecipes implements Action {
    readonly type = FETCH_RECIPES;
}

export class StoreRecipes implements Action {
    readonly type = STORE_RECIPES;
}

export class DeleteRecipe implements Action {
    readonly type = DELETE_RECIPE;

    constructor(public payload: number) {}
}

export class AddRecipe implements Action {
    readonly type = ADD_RECIPE;

    constructor(public payload: RecipeModel) {}
}

export class UpdateRecipe implements Action {
    readonly type = UPDATE_RECIPE;

    constructor(public payload: { index: number; newRecipe: RecipeModel }) {}
}


export type RecipesActions =
    | SetRecipes
    | FetchRecipes
    | AddRecipe
    | UpdateRecipe
    | DeleteRecipe
    | StoreRecipes;
