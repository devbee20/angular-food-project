import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { map, switchMap } from "rxjs/operators";

import { RecipeModel } from "../recipe.model";
import * as fromApp from '../../store/app.reducer';
import * as ShoppingListActions from "../../shopping-list/store/shopping-list.actions";
import * as  RecipesActions from "../store/recipe.actions";


@Component({
    selector: 'app-recipe-detail',
    templateUrl: './recipe-detail.component.html',
    styleUrls: ['./recipe-detail.component.scss']
})
export class RecipeDetailComponent implements OnInit {

    recipe: RecipeModel;
    id: number;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private store: Store<fromApp.AppState>
                ) {
    }

    ngOnInit(): void {
        this.route.params
            .pipe(
                map(params => {
                    return +params['id'];
                }),
                switchMap(id => {
                    this.id = id;
                    return this.store.select('recipes');
                }),
                map(recipesState => {
                    return recipesState.recipes.find((recipe, index) => {
                        return index === this.id;
                    });
                })
            )
            .subscribe(recipe => {
                this.recipe = recipe;
            });
    }

    onAddToShoppingList() {
        this.store.dispatch(
            new ShoppingListActions.AddIngredients(this.recipe.ingredients)
        );
    }

    onEditRecipe() {
        this.router.navigate(['edit'], { relativeTo: this.route });
    }

    onDeleteRecipe() {
        this.store.dispatch(new RecipesActions.DeleteRecipe(this.id));
        this.store.dispatch(new RecipesActions.StoreRecipes());
        this.router.navigate(['/recipes']);
    }

}
