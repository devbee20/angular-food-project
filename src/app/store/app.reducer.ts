import { ActionReducerMap } from "@ngrx/store";
import * as fromRecipes from '../recipes/store/recipe.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import * as fromShoppingList from '../shopping-list/store/shopping-list.reducer';

export interface AppState {
    recipes: fromRecipes.State;
    auth: fromAuth.State;
    shoppingList: fromShoppingList.State;
}

export const appReducer: ActionReducerMap<AppState> = {
    recipes: fromRecipes.recipeReducer,
    auth: fromAuth.authReducer,
    shoppingList: fromShoppingList.shoppingListReducer,
};
