# KITCHEN - Demo project built with Angular version 9.0.7.

## Project Overview

Kitchen is a demo Angular application covering Angular 2-9 features and NgRX state management.
Project focuses on the key features and skills required to successfully implement a modern angular application.
Implements fully functional CRUD operations for managing recipes and shopping list.

## Login/Register page

<div align="center">
<img src="./src/assets/images/login.png" />
</div>

## Main page
<div align="center">
<img src="./src/assets/images/main.png" />
</div>

## Key features

> NgRX state management

> AuthGuards

> Firebase Authentication and Database integration

> Forms

> Http requests

> Routing

> CRUD operations for recipes and shopping list

> Modules lazy loading

> Custom directives 

> Dynamic components

> Bootstrap 4.4.1, SASS (scss syntax)

## Project setup

1) Create Firebase account
2) Configure user authentication with Sign-in method: Email/Password 
3) Create firebase config on base of firebase.config.sample.ts

```
 /*
   Firebase setup config
   Paste your own config object into a file called firebase.config.ts in the app folder.
 */
 
 // Initialize Firebase
 export let firebaseConfig = {
   firebaseAPIKey: ' ',
   authDomain:  ' ',
   projectDomain: ' ',
 };

```
4) npm install
5) ng serve

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

